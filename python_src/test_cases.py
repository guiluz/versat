#! /usr/bin/python
#
#    t e s t _ c a s e s . p y
#

import sys
import string
import os
import subprocess

make = "make"
clean = "clean"
master_file = "xtop"

assembler = "./assembler.py "
src_path = "./../verilog_src/"
golden_path = "./golden_files/"
va_path = " ../assembly_src/"

option = [" < "," -b < "]

num_tests = 0
tests_list = []

fp = open(os.devnull, "w")

def diff(name,master_name,numb):
	if (numb == 0):
		fw = open("aux1.vcd","w")
		subprocess.call(["sed", "/$date/,/$end/g", src_path+name+".vcd"],stdout=fw)
		fw.close()
		fw = open("aux2.vcd","w")
		subprocess.call(["sed", "/$date/,/$end/g", golden_path+name+".vcd"],stdout=fw)
	else:
		fw = open("aux1.vcd","w")
		subprocess.call(["sed", "/$date/,/$end/g", src_path+master_name+".vcd"],stdout=fw)
		fw.close()
		fw = open("aux2.vcd","w")
		subprocess.call(["sed", "/$date/,/$end/g", golden_path+master_name+"_"+name+".vcd"],stdout=fw)
	fw.close()
	numb = subprocess.call(["diff","aux1.vcd", "aux2.vcd"],stdout=fp)
	subprocess.call(["rm","-rf","aux1.vcd"])
	subprocess.call(["rm","-rf","aux2.vcd"])
	return numb
	

fr = open("./test_cases", "r")

line = fr.readline()

while(not(line == "")):
	if (line[0] != '#' and str.isdigit(line[0])):
		tests_list.append(line)
		num_tests += 1
	line = fr.readline()
	

fr.close()

subprocess.call([make,"-C", src_path,clean])

fw = open("./report", "w")

i = 0

while(i < num_tests):
	test = tests_list[i]
	[test_type,name] = string.split(test)
	test_type_num = int(test_type)
	if(test_type_num <= 2 and test_type_num >= 0):
		if (test_type_num == 0):
			numb = subprocess.call([make,"-C",src_path,name])
		else:
			numb = os.system(assembler+option[test_type_num-1]+va_path+name+".va")
			if (numb != 0):
				result = ": Error at assemble file."
				fw.write("Test " + str(i+1) + " - " + name + result + "\n")
				i += 1
				continue
			else:
				numb = subprocess.call([make,"-C",src_path,master_file])
		if(numb != 0):
			result = ": Error at compile or run file."
		else:
			numb = diff(name,master_file,test_type_num)
			if (numb == 0):
				result = ": There are no differences -> passed"
			else:
				result = ": There are some differences -> failed"
	else:
		result = "invalid arguments at test_cases file.\n\ttest_type = " + str(test_type) + " , it should be a number between 0 and 3."
	fw.write("Test " + str(i+1) + " - " + name + result + "\n")
	i += 1
	

fw.close()

fp.close()

print 0
