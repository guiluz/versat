#!/usr/bin/python
#
#    a s s e m b l e r . p y
#
import sys, string

list_mem = ["./../assembly_src/opcode.hex","./../assembly_src/boot_rom.hex"]

class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

codes={"nop":0,"rdw":1, "wrw":2, "wrc":2, "rdwb":3, "wrwb":4, 
       "beqi":5, "beq":6, "bneqi":7, "bneq":8, "ldi":9, "ldih":10, 
       "add":11, "addi":12, "sub":13, "and":14, "stop":15}

lookup={"CTRL_REGF_BASE":0, "PROG_ADDR_REG":0,"NEW_WORD_STATUS_REG":1,"NEW_WORD_REG":2, 
	"DMA_CTRL_REG":64,"DMA_STATUS_REG":65,"DMA_EXT_ADDR":66,
	"PROG_MEM_BASE":2048, "PROG_START":256, 
        "CONF_MEM_BASE":4096, "CLEAR_CONFIG_ADDR":6143,
	"ENG_CTRL_REG":6144,"ENG_STATUS_REG":6145,"ENG_FU_BASE":6176, 
	"MEMA0":6176,"MEMB0":6177,"MEMA1":6178,"MEMB1":6179,
	"MEMA2":6180,"MEMB2":6181,"MEMA3":6182,"MEMB3":6183,
	"ALU0":6184,"ALU1":6185,"ALU2":6186,"ALU3":6187,
	"MULT0":6188,"MULT1":6189,"MULT2":6190,"MULT3":6191,
	"BS0":6192,"BS1":6193,
	"ENG_MEM_BASE":8192,
        "MEM_CONF_ITER_OFFSET":0,"MEM_CONF_PER_OFFSET":1,"MEM_CONF_DUTY_OFFSET":2,
        "MEM_CONF_SELA_OFFSET":3,"MEM_CONF_START_OFFSET":4,"MEM_CONF_END_LOOP_OFFSET": 5,"MEM_CONF_INCR_OFFSET":6,
        "MEM_CONF_INCR_TYPE_OFFSET":7,"MEM_CONF_DELAY_OFFSET":8,"MEM_CONF_RVRS_OFFSET":9,"MEM_CONF_OFFSET":10,
        "ALU_CONF_SELA_OFFSET":0,"ALU_CONF_SELB_OFFSET":1,"ALU_CONF_FNS_OFFSET":2,"ALU_CONF_OFFSET":3,
	"MUL_CONF_SELA_OFFSET":0,"MUL_CONF_SELB_OFFSET":1,"MUL_CONF_LONHI_OFFSET":2,
	"MUL_CONF_DIV2_OFFSET":3,"MUL_CONF_OFFSET":4,
        "BS_CONF_SELA_OFFSET":0,"BS_CONF_SELB_OFFSET":1,"BS_CONF_LNA_OFFSET":2,
        "BS_CONF_LNR_OFFSET":3,"BS_CONF_OFFSET":4,
        "MEM0A_CONFIG_ADDR":4096,"MEM0B_CONFIG_ADDR":4106,"MEM1A_CONFIG_ADDR":4116,"MEM1B_CONFIG_ADDR":4126,
	"MEM2A_CONFIG_ADDR":4136,"MEM2B_CONFIG_ADDR":4146,"MEM3A_CONFIG_ADDR":4156,"MEM3B_CONFIG_ADDR":4166,
	"ALU0_CONFIG_ADDR":4176,"ALU1_CONFIG_ADDR":4179,"ALU2_CONFIG_ADDR":4182,"ALU3_CONFIG_ADDR":4185,
        "MULT0_CONFIG_ADDR":4188,"MULT1_CONFIG_ADDR":4192,"MULT2_CONFIG_ADDR":4196,"MULT3_CONFIG_ADDR":4200,
	"BS0_CONFIG_ADDR":4204,"BS1_CONFIG_ADDR":4208,
	"s0":1,"s1":2,"sdata_in":3,
	"smem0A":4,"smem0B":5,"smem1A":6,"smem1B":7,
	"smem2A":8,"smem2B":9,"smem3A":10,"smem3B":11,
	"salu0":12,"salu1":13,"salu2":14,"salu3":15,
	"smul0":16,"smul1":17,"smul2":18,"smul3":19,
	"sbs0":20,"sbs1":21,"sdisabled":0,
	"ALU_LOGIC_OR":0,"ALU_LOGIC_AND":1,"ALU_LOGIC_ANDN":2,"ALU_LOGIC_XOR":3,
	"ALU_SEXT8":4,"ALU_SEXT16":5,"ALU_SHIFTR_ARTH":6,"ALU_SHIFTR_LOG":7,
	"ALU_CMP_UNS":13,"ALU_CMP_SIG":14,"ALU_ADD":16,"ALU_SUB":17,
	"ALU_PCBF":9,"ALU_PCE":10,"ALU_PCNE":11,"ALU_PCCLZ":12,
	"ALU_MAX":18,"ALU_MIN":19,"ALU_ABS":20	}


def getVal (s) :
    "return numeric value of a symbol or number"
    if not s : return 0       # Empty symbol - zero
    a = lookup.get(s)         # Get value or None if not in lookup
    if a == None : return int(s)  # Just a number
    else         : return a

def pass1 (program,pReg) :
    "determine addresses for labels and add to the lookup dictionary"
    global lookup
    for lin in program :
        flds = string.split(lin)
        if not flds : continue        # just an empty line
        if lin[0] == '#' : continue        # a comment line
        if lin[0] > ' ' :
            symb = flds[0]            # A symbol. Save its address in lookup
            lookup[symb] = pReg
            if len(flds) > 1 :        # Advance program counter unless only a symbol
                pReg = pReg + 1
        else : pReg = pReg + 1

def assemble (flds) :
    "assemble instruction to machine code"
    opval = codes.get(flds[0])
    if opval == None : raise MyError(1)     # just a number
    if len(flds) == 1 : return opval*16**7  
    parts  = string.split(flds[1],",")         # see if reg,address
    if len(parts) == 1 : return opval*16**7 + getVal(parts[0])
    if len(parts) == 2 and flds[0]=="wrc" : return  opval*16**7 + getVal(parts[0]) + getVal(parts[1])
    if len(parts) == 2 and flds[0]!="wrc" : return  opval*16**7 + getVal(parts[0])*16**4 + getVal(parts[1])
    if len(parts) == 3 : return  opval*16**7 + getVal(parts[0])*16**6 + getVal(parts[1])*16**4 + getVal(parts[2]) 
    if len(parts) > 3 : raise MyError(2)
    

def pass2 (program, pReg, mem_type) :
    "translate assembly code and symbols to machine code"
    fw = open(mem_type, 'w')
    j = 0
    for lin in program :
        flds = string.split(lin)
        if lin[0] == '#' : continue        # a comment line
        if lin[0] > ' ' : flds = flds[1:]     # drop symbol if there is one
        if not flds : print "            ", lin,   # print now if only a symbol
        else :
            try :
                instruction = assemble(flds)
                fw.write(str("{:08x}".format(instruction))+"\n")
                print "%03x %08x   %s" % (pReg, instruction, lin),
                pReg = pReg + 1
                j = j + 1
            except MyError as e:
                print "*** ******   %s" % lin,
    if  mem_type == list_mem[1]:
        while j < 254: # complete boot ROM file with 00000000 all 256 positions
            fw.write(str("{:08x}".format(0))+"\n")
            j = j + 1
        fw.write(str("{:08x}".format(0)))

def main () :

    pReg = 0x100
    mem_type = list_mem[0]
    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]
        if arg == "-b":
            print "Assembling a boot loader at address 0"
            pReg = 0
            i = i + 1
            mem_type = list_mem[1]
        else:
            print "Wrong argument: " + sys.argv[i]
            sys.exit(1)
        
    program = sys.stdin.readlines()
    pass1 (program, pReg)
    pass2 (program, pReg, mem_type)

if __name__ == "__main__" : main ()
