/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>
            Guilherme Luz <gui_luz_93@hotmail.com>
 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"

module xctrl(
	     input 			   clk,
	     input 			   rst,

	     //prog memory interface 
	     input [`DATA_W-1:0]           instruction,
	     output reg [`ADDR_W-1:0] 	   pc,

	     //read/write interface 
	     output reg 		             rw_req,
	     output reg 		   		     rw_rnw,
	     output reg [`INT_ADDR_W-1:0]    rw_addr,
	     output reg 			mem_wnr,
	     input [`DATA_W-1:0] 	   	     data_to_rd,
	     output reg [`DATA_W-1:0] 	     data_to_wr

	     );

 
 	reg [`DATA_W-1:0] regA;
 	reg [`DATA_W-1:0] regA_nxt;
 	reg [`DATA_W-1:0] regB;
 	reg [`DATA_W-1:0] regB_nxt;
 	reg [`INT_ADDR_W-1:0] addrint_reg;
 	reg [`ADDR_W-1:0] pc_nxt;
 	
	//instruction fields
 	wire [`OPCODESZ-1 :0] opcode;
 	wire [`INT_ADDR_W-1:0] addrint;
 	wire [`TRFSZ_W-1:0] size;
 	wire [`DATA_W-1:0] imm;
 	
 	//instruction field assignment
	assign opcode = instruction[`DATA_W-1 -:`OPCODESZ];
 	assign addrint = instruction[`INT_ADDR_W-1:0];
 	assign size = instruction[`INT_ADDR_W+`TRFSZ_W-1:`INT_ADDR_W];
 	assign imm = (instruction[27]==1'b0)? {4'b0,instruction[`DATA_W-5:0]} :{4'hF,instruction[`DATA_W-5:0]} ;

  
   //registers
   always @ (posedge clk, posedge rst)
     if(rst) begin 
		pc <= `ADDR_W'h0;
		regA <= `DATA_W'h0;
		regB <= `DATA_W'h0;
		addrint_reg <= `INT_ADDR_W'h0;
		mem_wnr <= 1'b0;
     end else begin
		regA <= regA_nxt;
		regB <= regB_nxt;
		pc <= pc_nxt;
		addrint_reg <= addrint;
	 end 	
   
   always @ * begin
	  regA_nxt=regA;
	  regB_nxt=regB;
      	  rw_req = 1'b0;
          rw_rnw = 1'b1;
          mem_wnr = 1'b0;
          rw_addr = addrint;
          data_to_wr = regA;
          pc_nxt = pc+1;
      
	  case (opcode)
		`RDW: begin 
			rw_req = 1'b1;
			regA_nxt = data_to_rd;
		end
		`RDWB: begin 
			rw_req = 1'b1;
			regB_nxt = data_to_rd;
		end	
		`WRW: begin 
			rw_req = 1'b1;
			rw_rnw = 1'b0;
		end
		`WRWB: begin 
			rw_req = 1'b1;
			rw_rnw = 1'b0;
			mem_wnr = 1'b1;
			rw_addr[`INT_ADDR_W-1:0] = regB[`INT_ADDR_W-1:0];
			data_to_wr = regA;
			regB_nxt=regB+1;
		end	
		`BEQI: begin
			regA_nxt = regA - `DATA_W'd1;
			if ( regA[`DATA_W-1:0] == `DATA_W'd0 )
				pc_nxt = addrint[`ADDR_W-1:0];
		end		
		`BEQ: begin
			regA_nxt = regA - `DATA_W'd1;
			if ( regA[`DATA_W-1:0] == `DATA_W'd0 )
				pc_nxt = data_to_rd[`ADDR_W-1:0];
		end											
		`BNEQI: begin
			regA_nxt = regA - `DATA_W'd1;
			if ( regA[`DATA_W-1:0] != `DATA_W'd0 )
				pc_nxt = addrint[`ADDR_W-1:0];
		end
		`BNEQ: begin
			regA_nxt = regA - `DATA_W'd1;
			if ( regA[`DATA_W-1:0] != `DATA_W'd0 )
				pc_nxt = data_to_rd[`ADDR_W-1:0];
		end
		`LDI: begin
			regA_nxt =imm;
		end		
		`LDIH: begin
			regA_nxt={imm[15:0],16'b0};
		end		
		`ADD: begin 
			rw_req=1'b1;
			regA_nxt = regA + data_to_rd;
		end 
		`SUB: begin 
			rw_req=1'b1;
			regA_nxt = regA - data_to_rd;
		end
		`ADDI: begin
			regA_nxt = regA + imm;
		end
		`AND: begin 
			regA_nxt = regA & data_to_rd;
		end 
		`NOP: begin 
		end 
		`STOP: begin
			rw_req = 1'b1;
			rw_rnw = 1'b0;
			pc_nxt = `ADDR_W'h0;
			rw_addr = `PROG_ADDR_REG;
			data_to_wr = `DATA_W'h0;
		end		
		default: 
			pc_nxt = pc;
				
	  endcase
   end 						
endmodule
