/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: xalu testbench

 Copyright (C) 2014 Authors

 Author(s): Guilherme Luz <gui_luz_93@hotmail.com>

 ***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xaludefs.v"
`include "xconfig.v"

module xalu_tb;
      parameter clk_per = 10;
      integer signed k;
      integer file;
      // control 
      reg clk;
      reg rst;
 
      //controller interface
      reg rw_req;
      reg rw_rnw;
      reg [`DATA_W-1:0] rw_data_to_wr;

      //data
      reg [`N*`DATA_W-1:0] data_in_bus;
      wire  [31:0] alu_result;
      wire 	 c_out;
	
      //config data
      reg [`ALU_CONFIG_BITS - 1:0] configdata;

      xalu uut (
              .clk(clk),
              .rst(rst),
	      .rw_req(rw_req),
	      .rw_rnw(rw_rnw),
   	      .rw_data_to_wr(rw_data_to_wr),
	      .data_in_bus(data_in_bus),
	      .alu_result(alu_result),
	      .c_out(c_out),
	      .configdata(configdata)
       );

      reg [31:0]  result_compare;
      wire [5:0]  pcmp_comp;
      reg [31:0]  opa_comp;
      reg [31:0]  opb_comp;
      reg [2:0]   byte_find_comp;
      integer     err_tb ;

      initial begin
         $dumpfile("alu.vcd");
         $dumpvars();
         // Initialize Inputs
         err_tb=0;
         clk = 0;
         rw_req=0;
         rw_rnw=0;
         k=-1;
         rw_data_to_wr=32'h0000;
         rst=1;
         configdata[4:0]=`ALU_LOGIC_OR;
         configdata[14-:5]=`s0;
         configdata[9-:5]=`s1;
         data_in_bus[671-:32]=32'd26;
         data_in_bus[(671-32)-:32]=-32'd25;
         #(clk_per/2+1);
         //reset tested, turn off reset
         rst=0;

         //rw_req=1 and rw_rnw=0
         rw_req=1;
         rw_data_to_wr=32'd20;
         #clk_per
         rst=0;
         rw_rnw=1;

         //test ALU, PATTERN COMPARE, EXT2 functions 
         repeat(20) begin
            #clk_per
            k=k+1;
            configdata[4:0]=configdata[4:0]+1;
         end
         #clk_per
         k=k+1;

          file = $fopen ("xalu_tb_result.txt", "w");
          if(err_tb==1)
             $fwrite (file, "Failed");
          else
             $fwrite (file, "Passed");

         #clk_per $finish;
      end

      always @ (posedge clk) begin
        opa_comp <= data_in_bus[671-:32];
        opb_comp <= data_in_bus[(671-32)-:32];
        if (rst == 1'b1)
           result_compare=32'd0;
        else
           if ( rw_req & ~rw_rnw )
              result_compare <= rw_data_to_wr;
           else
              case(configdata[4:0])
                    `ALU_LOGIC_OR:        result_compare <= opa_comp | opb_comp ;

                    `ALU_LOGIC_AND:       result_compare <= (opa_comp & opb_comp ) ;
 
                    `ALU_LOGIC_ANDN:      result_compare <= (opa_comp & ~opb_comp ) ;

                    `ALU_LOGIC_XOR:       result_compare <= (opa_comp ^ opb_comp ) ;

                    `ALU_SEXT8:           result_compare[31:0] <= {{24{opa_comp[7]}}, opa_comp[7:0]}; 
                
                    `ALU_SEXT16:          result_compare <= {{16{opa_comp[15]}}, opa_comp[15:0]} ;

                    `ALU_SHIFTR_ARTH :    begin
                                            result_compare <= ( opa_comp>>1);
                                            result_compare[31] <= opa_comp[31];
                                          end
                 
                    `ALU_SHIFTR_LOG :     result_compare <= ( opa_comp>>1) ;

                    `ALU_CMP_UNS :        result_compare <= opb_comp + ~opa_comp +1;

                    `ALU_CMP_SIG :        result_compare <= opb_comp + ~opa_comp +1;

                    `ALU_ADD :            result_compare <=  opa_comp + opb_comp;

                    `ALU_SUB :            result_compare <=  opb_comp - opa_comp;

                    `ALU_PCBF :           result_compare <= byte_find_comp;
                 
                    `ALU_PCE  :           result_compare <= (opa_comp == opb_comp) ;

                    `ALU_PCNE :           result_compare <= (opa_comp != opb_comp) ;

                    `ALU_PCCLZ :          result_compare <= pcmp_comp;

                    `ALU_MAX   :          if( opa_comp[31]==1'b1 ^ opb_comp[31]==1'b1)
                                             if ( (opa_comp < opb_comp) )
                                             result_compare <= opa_comp;
                                             else 
                                                result_compare <= opb_comp;
                                          else
                                             if ( (opa_comp > opb_comp) )
                                                result_compare <= opa_comp;
                                             else 
                                                result_compare <= opb_comp;

                    `ALU_MIN    :         if( opa_comp[31]==1'b1 ^ opb_comp[31]==1'b1)
                                             if ( (opa_comp > opb_comp) )
                                                result_compare <= opa_comp;
                                             else 
                                                result_compare <= opb_comp;
                                          else
                                             if ( (opa_comp < opb_comp) )
                                                result_compare <= opa_comp;
                                             else 
                                                result_compare <= opb_comp;

                    `ALU_ABS :            if ( opa_comp[31] ==1'b1 ) begin
                                             result_compare <= ~opa_comp+1;
                                          end else
                                             result_compare <= opa_comp;
            
              endcase
      end    
      
      xclz clz(
               .data_in(opa_comp),
	       .data_out(pcmp_comp));

      always @(opa_comp or opb_comp) begin
         if((opa_comp[31:24]  == opb_comp[31:24] )) begin
            byte_find_comp = 3'b 001;
         end
         else if((opa_comp[23:16]  == opb_comp[23:16] )) begin
            byte_find_comp = 3'b 010;
         end
         else if((opa_comp[15:8]  == opb_comp[15:8] )) begin
            byte_find_comp = 3'b 011;
         end
         else if((opa_comp[7:0]  == opb_comp[7:0] )) begin
            byte_find_comp = 3'b 100;
         end
         else begin
            byte_find_comp = 3'b 000;
         end
      end

      always @ (negedge clk)
         if (result_compare != alu_result) begin
            if(k!=32'd8 && k!=32'd15 && k<=32'd20)  // there are not operations 32'd8, 32'd15 or >32'd20
               if(k!=13 || (opa_comp[31]==0 && opb_comp[31]==0)) begin //ALU_CMP_UNS is invalid with negative numbers
                  $display ("ERROR AT TIME%d , OPERATION:%d ",$time , k);
                  $display ("Expected value %d, Got Value %d", result_compare, alu_result);
                  err_tb = 1;
               end
            end


      always
         #(clk_per/2)  clk =  ~ clk;

endmodule
