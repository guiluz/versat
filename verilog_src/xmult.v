/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xmult (
    input clk,
    input rst,
	
        //controller interface
        input rw_req,
	input rw_rnw,
	input [`DATA_W-1:0] rw_data_to_wr,

	//data 
	input [`N*`DATA_W-1:0] data_in_bus,
   output reg signed [`DATA_W-1:0] product,
	 
	//config 
	input [`MULT_CONFIG_BITS-1:0] configdata
	
    );

	wire signed [2*`DATA_W-1:0] res1;
	wire signed [`DATA_W-1:0] res2;
	wire signed [`DATA_W-1:0] res3;

	//data  
	wire signed [`DATA_W-1:0] op_a;
   wire signed [`DATA_W-1:0] op_b;
	 
	//config data
	wire [`N_W-1: 0] sela;
	wire [`N_W-1: 0] selb;
   wire lonhi;
   wire div2;
	
	wire enabledA, enabledB;

	//unpack config bits 
	assign sela = configdata[`MULT_CONFIG_BITS-1 -: `N_W];
	assign selb = configdata[`MULT_CONFIG_BITS-1-`N_W -: `N_W];
	assign lonhi = configdata[1];
	assign div2 = configdata[0];
	
	//input selection 
	xinmux muxa (
		.sel(sela),
		.data_in_bus(data_in_bus),
		.data_out(op_a),
		.enabled(enabledA)
	);
	
	xinmux muxb (
		.sel(selb),
		.data_in_bus(data_in_bus),
		.data_out(op_b),
		.enabled(enabledB)		
	);
	
	
	assign res1 = op_a * op_b;
	assign res2 = (lonhi == 1'b1)? res1[`DATA_W-1:0] : res1[2*`DATA_W-1:`DATA_W];
	assign res3 = (div2 == 1'b0)? res2<<1 : res2;

	always @ (posedge clk) 
		if (rst)
		  product <= `DATA_W'h00000000;
		else begin
	           if (rw_req & ~rw_rnw)
		     product <= rw_data_to_wr;
		   else if(enabledA & enabledB)
		     product <= res3;
		end
endmodule
