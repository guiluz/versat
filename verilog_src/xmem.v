/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>

***************************************************************************** */

`timescale 1ns / 1ps
`include "./xdefs.v"


module xmem(

	 //control 
	 input 				 clk,
	 input 				 rstA,
	 input 				 rstB,
	 input 				 en_A,
	 input 				 en_B,
	 output [1:0] 			 done,

	 //controller interface
	 input 				 rw_addrA_req,
	 input 				 rw_addrB_req,
	 
	 input 				 rw_mem_req,
	 input 				 rw_rnw,
	 input [`ADDR_W-1:0] 		 rw_addr,
	 input [`DATA_W-1:0] 		 rw_data_to_wr,
	 
	 //dma interface
 	 input 				 dma_rnw,
	 input [`ADDR_W-1:0] 		 dma_addr,
	 input [`DATA_W-1:0] 		 dma_data_in,
	 input 				 dma_mem_req,
 
	 //input / output data
	 input [`N*`DATA_W-1:0] 	 data_in_bus,
	 output [`DATA_W-1:0] 		 outA,
	 output [`DATA_W-1:0] 		 outB,
	 
	 //configurations
	 input [2*`MEMP_CONFIG_BITS-1:0] config_bits
	 
    );

	function [`ADDR_W-1:0] reverseBits;    
	  input [`ADDR_W-1:0] word;             
	  integer i;
	  
	  begin
		for (i=0; i < `ADDR_W; i=i+1)
				reverseBits[i]=word[`ADDR_W-1 - i];         
	  end                           
	endfunction    
	
	wire doneA;
	wire doneB;

	//memory
	reg [`DATA_W-1:0] mem [(2**`ADDR_W)-1:0];

	//port addresses and enables 
    wire [`ADDR_W-1:0] addrA;
    wire [`ADDR_W-1:0] addrB;
	wire [`ADDR_W-1:0] addrA_int;
    wire [`ADDR_W-1:0] addrB_int;
	wire enA, enB;

	//data inputs
    wire [`DATA_W-1:0] inA;
    wire [`DATA_W-1:0] inB;

	//configuration
	wire [`PERIOD_W-1:0] perA;
	wire [`PERIOD_W-1:0] dutyA;
	wire [`ADDR_W-1:0] iterationsA;
        
	wire [`N_W-1:0] selA;
	wire [`ADDR_W-1:0] startA;
        wire [`ADDR_W-1:0] endA;
	wire [`ADDR_W-1:0] incrA;
	wire [`PERIOD_W-1:0] delayA;
	wire rnwA;
	wire reverseA;
        
        wire [`PERIOD_W-1:0] perB;
	wire [`PERIOD_W-1:0] dutyB;
	wire [`ADDR_W-1:0] iterationsB;
        
	wire [`N_W-1:0] selB;
	wire [`ADDR_W-1:0] startB;
        wire [`ADDR_W-1:0] endB;
	wire [`ADDR_W-1:0] incrB;
	wire [`PERIOD_W-1:0] delayB;
	wire rnwB;	
	wire reverseB;	
	
	wire in_enA, in_enB, enA_int, enB_int;
	reg [`DATA_W-1:0] outA_int, outB_int;
	wire [`DATA_W-1:0] data_to_wrA;
	wire [`DATA_W-1:0] data_to_wrB;

	wire incr_typeA;
        wire incr_typeB;

	//done signal
	assign done = {doneA,doneB};
	
	//unpack config data
	assign iterationsA = config_bits[2*`MEMP_CONFIG_BITS-1 -: `ADDR_W];
	assign perA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-1 -: `PERIOD_W];
	assign dutyA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-`PERIOD_W-1 -: `PERIOD_W];
	
	assign selA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-1 -: `N_W];
	assign startA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W];
        assign endA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-`ADDR_W-1 -: `ADDR_W];
	assign incrA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-2*`ADDR_W-1 -: `ADDR_W];
        assign incr_typeA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-3*`ADDR_W-1 -: 1];
	assign delayA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-3*`ADDR_W-1-1 -: `PERIOD_W];
	assign reverseA = config_bits[2*`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-3*`ADDR_W-`PERIOD_W-1-1 -: 1];
        
        assign iterationsB = config_bits[`MEMP_CONFIG_BITS-1 -: `ADDR_W];
	assign perB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-1 -: `PERIOD_W];
	assign dutyB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-`PERIOD_W-1 -: `PERIOD_W];
        
	assign selB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-1 -: `N_W];
	assign startB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W];
        assign endB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-`ADDR_W-1 -: `ADDR_W];
	assign incrB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-2*`ADDR_W-1 -: `ADDR_W];
        assign incr_typeB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-3*`ADDR_W-1 -: 1];
	assign delayB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-3*`ADDR_W-1-1 -: `PERIOD_W];
	assign reverseB = config_bits[`MEMP_CONFIG_BITS-`ADDR_W-2*`PERIOD_W-`N_W-3*`ADDR_W-`PERIOD_W-1-1 -: 1];

	assign enA = (dma_mem_req) |rw_mem_req | enA_int;
	assign enB = rw_mem_req | enB_int;

	assign rnwA = (dma_mem_req)?  dma_rnw : ~in_enA;
	assign rnwB = ~in_enB | ~rw_rnw;
	
	assign outA = (dma_mem_req)? outA_int :rw_addrA_req? {{(`DATA_W-`ADDR_W){1'b0}}, addrA} : outA_int;
	assign outB = rw_addrB_req? {{(`DATA_W-`ADDR_W){1'b0}}, addrB}: outB_int;
	
        assign data_to_wrA = (dma_mem_req & ~dma_rnw)? dma_data_in : inA ;
	assign data_to_wrB = (rw_mem_req & ~rw_rnw)?  rw_data_to_wr : inB;
	
	
	
	//input selection 
	xinmux muxa (
		.sel(selA),
		.data_in_bus(data_in_bus),
		.data_out(inA),
		.enabled(in_enA)
	);	
	
	xinmux  muxb (
		.sel(selB),
		.data_in_bus(data_in_bus),
		.data_out(inB),
		.enabled(in_enB)		
	);

	//address generators
	addrgen addrgenA (
		.clk(clk), 
		.rst(rstA), 
		.en(en_A), 
		.iterations(iterationsA), 
		.period(perA), 
		.duty(dutyA), 
		.start(startA),
		.end_loop(endA),
		.incr(incrA),
		.incr_type(incr_typeA),
		.delay(delayA), 
		.addr(addrA_int), 
		.mem_en(enA_int), 
		.done(doneA)
	);

	addrgen addrgenB (
		.clk(clk), 
		.rst(rstB), 
		.en(en_B), 
		.iterations(iterationsB), 
		.period(perB), 
		.duty(dutyB), 
		.start(startB),
		.end_loop(endB),
		.incr(incrB),
		.incr_type(incr_typeB),
		.delay(delayB), 
		.addr(addrB_int), 
		.mem_en(enB_int), 
		.done(doneB)
	);


   initial
      $readmemh("xmemdata.hex", mem, 0, 153);

		
	assign addrA = (dma_mem_req)? dma_addr[`ADDR_W-1:0] : (reverseA == 1'b1)? reverseBits(addrA_int) : addrA_int; 
	assign addrB = rw_mem_req? rw_addr :
						(reverseB == 1'b1)? reverseBits(addrB_int) : addrB_int; 

	//memory implementations
   always @(posedge clk)
      if (enA) begin
         if (~rnwA )
            mem[addrA] <= data_to_wrA;
         outA_int <= mem[addrA];
      end
      
	//PORT B
   always @(posedge clk)			
      if (enB) begin
         if (~rnwB)
            mem[addrB] <= data_to_wrB;
         outB_int <= mem[addrB];
      end
   

endmodule
