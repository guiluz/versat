/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module  xinmux (
    input [`N_W-1:0] sel,
    input [`DATA_BITS-1:0] data_in_bus,
    output reg [`DATA_W-1:0] data_out,
	 output reg enabled
    );


always @ (*) begin : input_data_mux

	integer i;
	enabled = 1'b1;
	data_out = data_in_bus[`DATA_BITS-`DATA_W*(`s0-1)-1 -: `DATA_W ];
	
	if (sel == `s0 )
		data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`s0-1)-1 -: `DATA_W];
	else if (sel == `s1)	
		data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`s1-1)-1 -: `DATA_W ];
	else if (/*side == `sideA &&*/ sel == `sdata_in )
		data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`sdata_in-1)-1 -: `DATA_W];
	else begin 
		for (i=0; i < `Nmem; i = i+1) begin
			if ( sel == `smem0A+2*i ) 
				data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`smem0A+2*i-1)-1 -: `DATA_W];
			if ( sel == `smem0B+2*i ) 
				data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`smem0B+2*i-1)-1 -: `DATA_W];
		end 
	
		for (i=0; i < `Nalu; i = i+1)
			if ( sel == `salu0+i ) 
				data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`salu0+i-1)-1 -: `DATA_W];
		
		for (i=0; i < `Nmul; i = i+1)
			if ( sel == `smul0+i ) 
				data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`smul0+i-1)-1 -: `DATA_W];

		for (i=0; i < `Nbs; i = i+1)
			if ( sel == `sbs0+i ) 
				data_out =  data_in_bus[`DATA_BITS-`DATA_W*(`sbs0+i-1)-1 -: `DATA_W];
		if (sel == `sdisabled )	
				enabled = 1'b0;
	end
end 

endmodule
