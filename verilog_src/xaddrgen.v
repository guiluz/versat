/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "./xdefs.v"

module addrgen(
    input 		       clk,
    input 		       rst,
    input 		       en,
    input [`ADDR_W - 1:0]      iterations,
    input [`PERIOD_W - 1:0]    period,
    input [`PERIOD_W - 1:0]    duty,
    input [`PERIOD_W - 1:0]    delay,
    input [`ADDR_W - 1:0]      start,
    input [`ADDR_W - 1:0]      end_loop,
    input [`ADDR_W - 1:0]      incr,
    input 		       incr_type,
    output reg [`ADDR_W - 1:0] addr,
    output reg 		       mem_en,
    output reg 		       done
    );

reg signed [`PERIOD_W - 1:0] per_cnt; //period count
reg [`ADDR_W - 1:0] iter; //iterator
wire  active;

assign active = (iterations == `PERIOD_W'd0)? 1'b0 : 1'b1;

always @ (posedge clk) begin 

	if(rst) begin 
		per_cnt <= -delay;
		addr <= start;
		iter <= `ADDR_W'd0;
		mem_en <= 1'b0;
		done <= 1'b1;

		if (delay == `PERIOD_W'd0) begin
			iter <= `ADDR_W'd1;
			mem_en <= 1'b1;
		end
			
		//reset done -- generator enabled	
		if(active)
			done <= 1'b0; 

	end else begin		
		if(active & en) begin
		
			if (per_cnt == (period - `PERIOD_W'b1) || per_cnt == -`PERIOD_W'b1) begin 
				if(iter != iterations) begin 
					per_cnt <= `PERIOD_W'd0;
					mem_en <= 1'b1;
					iter <= iter + `ADDR_W'd1;
					if(per_cnt == (period - `PERIOD_W'b1)) begin
						if (addr == end_loop && end_loop != 0)
							addr <= start;
						else begin
						   if (incr_type) //incr_type 1 if it's a different incr, 0 if it's just a regular incr
						     addr <= addr - (duty - 2);
						   else
						     addr <= addr + incr;
						end
					end
				end 
			end else 	
				per_cnt <= per_cnt+`PERIOD_W'b1;
		
			if(mem_en) begin
				if (addr == end_loop && end_loop != 0)
				  addr <= start;
			        else
				  addr <= addr + incr;
			end
		
			if ((period != `PERIOD_W'd1 || iter == iterations ) && per_cnt == (duty - `PERIOD_W'b1)) begin
			//ensures that completion occurs when period=1
				mem_en <= 1'b0;
				addr <= addr;
				if(iter == iterations)
					done <= 1'b1;
			end 	
		
		end
	end 	
end 
endmodule
