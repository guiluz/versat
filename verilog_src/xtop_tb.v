/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
             Guilherme Luz <gui_luz_93@hotmail.com>
***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"

module xtop_tb;

	//parameters 
	parameter clk_period = 10;
   	parameter sclk_per=10*16.5;
	integer i;
    	reg clk;
    	reg rst;
	reg en;
	 
	//control slave interface to master CPU 
	 wire                          spi_we;
	 wire [`CTRL_REGF_ADDR_W-1:0] spi_addr;
	 wire [`DATA_W-1:0]           spi_data_in;
	 wire [`DATA_W-1:0]           spi_data_out;

	//spi
	reg ss;
	reg sclk;
	reg mosi;
	wire miso;
	reg getdata;

	//master interface to external memory
	 wire                 mreq;
	 wire                  ack;
	 wire                 mrnw;	
	 wire [`DATA_W-1:0]   maddr;
	 wire  [`DATA_W-1:0]   mdata_in;
	 wire [`DATA_W-1:0]   mdata_out;
	 wire [3:0]  		    msel;
	 wire [1:0]  		    mbte;
	 wire [2:0]  		    mcti;
	 wire        		    mcyc;
 
	//data direct IO
/*	 input                data_in_req,
	 output               data_in_rdy,
	 input [`DATA_W-1:0]  data_in,
	 output               data_out_req,
	 input                data_out_rdy,	 
	 output [`DATA_W-1:0] data_out*/

	// Instantiate the Unit Under Test (UUT)
	xtop uut (
		.clk(clk), 
		.rst(rst),
		.en(en),
		// control slave interface
		.spi_we(spi_we),
		.spi_addr(spi_addr),
		.spi_data_in(spi_data_in),
		.spi_data_out(spi_data_out),
		//external memory interface
		.mreq(mreq),
		.mack(ack),
		.mrnw(mrnw),	
		.maddr(maddr),
		.mdata_in(mdata_in),
	 	.mdata_out(mdata_out),
		.msel(msel),
		.mbte(mbte),
		.mcti(mcti),
		.mcyc(mcyc)
		//data direct IO
/*		.data_in_req(data_in_req),
		.data_in_rdy(data_in_rdy),
		.data_i(data_i),
		.data_out_req(data_out_req),
		.data_out_rdy(data_out_rdy),	 
		.data_out(data_out)*/
		);

	ram_wb_b3 mem_ext (
		.wb_clk_i(clk),
		.wb_rst_i(rst),
		.wb_adr_i(maddr),
		.wb_dat_i(mdata_out),
		.wb_sel_i(msel),
		.wb_we_i(~mrnw),
		.wb_bte_i(mbte),
		.wb_cti_i(mcti),
		.wb_cyc_i(mcyc),
		.wb_stb_i(mreq),
		.wb_ack_o(ack),
		.wb_dat_o(mdata_in)
	);

	spi_slave spi_slave (
	       	.clk(clk),
	       	.rst(rst),
	       	.sclk(sclk), 
	       	.ss(ss), 
	       	.mosi(mosi), 
	       	.miso(miso), 
	       	.data_in(spi_data_out),
	       	.data_out(spi_data_in),
		.address(spi_addr),
		.we(spi_we)
	);

	initial begin
		$dumpfile("xtop.vcd");
      		$dumpvars();
		// Initialize Inputs
		clk=1; 
		rst=0;
		en=1;
		#(clk_period)
		rst=1;

		#(clk_period)
		rst=0;
		ss=1;
		getdata=1;
		$vpi_rw(sclk, ss,mosi);

	        for (i = 0; i <4*38*(200); i=i+1) begin
		   $vpi_rw(sclk, ss, mosi);
		   #(sclk_per/2);
		end
		// Wait 100 ns for global reset to finish
		#100;
        	
		// Add stimulus here
		#(10000) $finish;

	end
      	always 
		#(clk_period/2) clk = ~clk;
	
endmodule
