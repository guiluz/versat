/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
***************************************************************************** */

`timescale 1ns / 1ps

`include "./xdefs.v"
`include "./xmem_map.v"

module xeng_addr_decoder (
	input 			rw_req,
	input [`INT_ADDR_W-1:0] rw_addr,
	input 			dma_req,
	input [1:0] dma_addr_12_11,
	
	output reg 		ctrl_req,
	output reg 		status_req,
	output reg [`N-4:0] 	fu_regf_req,
	output reg [`Nmem-1:0] 	mem_req,
	output reg [`Nmem-1:0] 	dma_mem_req    
	);

	always @ (*) begin : addr_decoder
		
		reg [`INT_ADDR_W-1:0] i;
		integer mark;
		
		ctrl_req = 1'b0;
		status_req = 1'b0;	
		mem_req = `Nmem'b0;
		fu_regf_req = {`N {1'b0}};
		
		mark = 0;
	
		for (i=0; i < `Nmem; i = i+1)
			dma_mem_req[i]=(dma_req==1'b1 && dma_addr_12_11==i)? 1'b1 : 1'b0;
		
		if (rw_addr == `ENG_CTRL_REG )
			ctrl_req = rw_req;
		else if (rw_addr == `ENG_STATUS_REG) 
			status_req = rw_req;				
		else if (rw_addr <= `ENG_MEM_BASE) begin 
			for (i=0; i < (`N-3); i = i+1)
				fu_regf_req[`N-4-i] = (rw_addr == (i+`ENG_FU_BASE) )? rw_req : 1'b0;
		end 			
		else begin
			for (i=0; i < `Nmem; i = i+1)
				if (mark == 0 && rw_addr <= `ENG_MEM_BASE+ (i+1)*2**`ADDR_W ) begin
					mem_req[i] = rw_req;	
					mark = 1'b1;
				end
		end
	end 

endmodule
