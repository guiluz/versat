/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"
module xdma_tb;

	//parameters 
	parameter clk_period = 20;

	// Inputs
	reg 	   			    rw_req;
	reg [`INT_ADDR_W-1:0]   rw_addr;
	reg 	   			    rw_rnw;
		//xdma
	reg 	   			    clk;
	reg 	   			    rst;
	reg 	   			    ext_ack;		
	reg [`DATA_W-1:0]       ext_data_in;
	reg [`DATA_W-1:0]       int_data_in;
	reg [`DATA_W-1:0]	    data_to_wr;
	reg 					mem_rdy;

		//xdata_eng
	reg   	                en;
	reg  [`CONFIG_BITS-1:0] config_bus;
	reg  [`DATA_W-1:0]      rw_data_to_wr;
	wire [`DATA_W-1:0]	    rw_data_to_rd;
	
		//instruction mem
	reg	 [`ADDR_W-1:0]		pc;

	// Outputs xdma
	wire [`DATA_W-1:0]	    data_to_rd;
	wire 				    ext_req;
	wire   				    int_en;
	wire 				    int_rnw;
	wire     			    ext_rnw;
	wire [`DATA_W-1:0]      int_data_out;
	wire [`DATA_W-1:0] 	    ext_data_out;
	wire [`INT_ADDR_W-2:0]  int_addr;
	wire [32-1:0]  		    ext_addr;
	wire [3:0]  		    ext_sel;
	wire 	    		    ext_cyc;
	wire [1:0]  		    ext_bte;
	wire [2:0]  		    ext_cti;
	wire					inst_mem_we;
	wire [`ADDR_W-1:0]		mem_addr;
	wire 					conf_req;
	wire 					conf_rnw;
	
	//Wires	
	wire	    		    ereq,ireq;
	wire        		    cyc;
	wire        		    ack;
	wire 	    		    ernw,irnw;
	wire [32-1:0] 		    eaddr;
	wire [`INT_ADDR_W-2:0]  iaddr;
	wire [`DATA_W-1:0]	    edata_in,idata_in;
	wire [`DATA_W-1:0] 	    edata_out,idata_out;
	wire [3:0]  		    sel;
	wire [1:0]  		    bte;
	wire [2:0]  		    cti;
	wire 					memrdy;

	// Instantiate the Unit Under Test (UUT)
	xdma uut (
		.clk(clk), 
		.rst(rst),
		//xctrl interface
		.rw_req(rw_req), 
		.rw_addr(rw_addr), 
		.data_to_rd(data_to_rd),
		.data_to_wr(data_to_wr),


		//external interface
		.ext_req(ereq),
		.ext_cyc(cyc),
		.ext_ack(ack),
		.ext_rnw(ernw),
		.ext_addr(eaddr),
		.ext_data_in(edata_in),
		.ext_data_out(edata_out),
		.ext_sel(sel),
		.ext_bte(bte),
		.ext_cti(cti),

		//internal interface
		.mem_rdy(memrdy),
     	.int_en(ireq),
		.int_rnw(irnw),
		.int_addr(iaddr),
		.int_data_in(idata_in),
		.int_data_out(idata_out),
		.inst_mem_we(inst_mem_we),
		.mem_addr(mem_addr),
		.conf_req(conf_req),
		.conf_rnw(conf_rnw)

		
	);
	ram_wb_b3 mem_ext (
		.wb_clk_i(clk),
		.wb_rst_i(rst),
		.wb_adr_i(eaddr),
		.wb_dat_i(edata_out),
		.wb_sel_i(sel),
		.wb_we_i(~ernw),
		.wb_bte_i(bte),
		.wb_cti_i(cti),
		.wb_cyc_i(cyc),
		.wb_stb_i(ereq),
		.wb_ack_o(ack),
		.wb_dat_o(edata_in)
	);
	
	xdata_eng data_eng(
		.clk(clk), 
		.en(en),
		.config_bus(config_bus), 
		.rw_req(rw_req), 
		.rw_rnw(rw_rnw), 
		.rw_addr(rw_addr), 
		.rw_data_to_wr(rw_data_to_wr), 
		.rw_data_to_rd(rw_data_to_rd),
		.dma_req(ireq),
		.dma_rnw(irnw),
		.dma_addr(iaddr),
		.dma_data_in(idata_out),
		.dma_data_out(idata_in),
		.mem_rdy(memrdy)
	);
	
	xinstr_mem instr_mem(
		.clk(clk),
		.pc(pc),
		.dma_addr(mem_addr),
		.dma_data(edata_in),
		.dma_wnr(inst_mem_we)
	);
	initial begin
		$dumpfile("xdma.vcd");
      		$dumpvars();
		// Initialize Inputs
		clk=1; 
		rst=0;
		en=1;
		config_bus=0;
		rw_req = 0;
		rw_rnw = 1;
		rw_addr = 0;
		rw_data_to_wr = 0;

		#(clk_period + 1)
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
        rw_req = 1;
		rw_data_to_wr = 1; //init
		data_to_wr=1;
		rst=1;
	
		#clk_period 
		rw_data_to_wr = 2; //run
		data_to_wr=2;
		#clk_period 		

		rst=0;
		
		//STORE TO WB
		#(clk_period)
		rw_addr=`DMA_CTRL_REG;
		data_to_wr={4'b0,6'b1,8'd20,14'd8192};
		rw_data_to_wr={4'b0,6'b1,8'd20,14'd10000};
		
		#(clk_period)
		rw_addr=`DMA_EXT_ADDR;
		data_to_wr={4'b0,28'd4000};
		rw_data_to_wr={4'b0,28'd4000};		
		
		#(clk_period)
		rw_addr = `DMA_STATUS_REG;
		
		//LOAD TO INST MEM
	    wait (data_to_rd==1) #clk_period;
		#(clk_period+1)
		rw_addr=`DMA_CTRL_REG;
		data_to_wr={4'b0,6'b1,8'd20,14'd2048};
		rw_data_to_wr={4'b0,6'b1,8'd20,14'd10000};
		
		#(clk_period)
		rw_addr=`DMA_EXT_ADDR;
		data_to_wr={4'b0,28'd4000};
		rw_data_to_wr={4'b0,28'd4000};


		//LOAD TO CONFIG MEM
		wait (data_to_rd==1) #clk_period;
		#(clk_period+1)
		rw_addr=`DMA_CTRL_REG;
		data_to_wr={4'b0,6'b0,8'd20,14'd6200};
		rw_data_to_wr={4'b0,6'b1,8'd20,14'd4096};
		
		#(clk_period)
		rw_addr=`DMA_EXT_ADDR;
		data_to_wr={4'b0,28'd4000};
		rw_data_to_wr={4'b0,28'd4000};
		pc=0;

		
		// Wait 100 ns for global reset to finish
		#100;
        	
		// Add stimulus here
		#(500) $finish;

	end
      	always 
		#(clk_period/2) clk = ~clk;
	
endmodule

