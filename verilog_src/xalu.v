/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias Lopes <joao.d.lopes91@gmail.com>

 ***************************************************************************** */
`timescale 1ns / 1ps

`include "xdefs.v"
`include "xaludefs.v"
`include "xconfig.v"

module xalu(
	    // control 
	    input 			   clk,
	    input 			   rst, 

            //controller interface
	    input 			   rw_req,
	    input 			   rw_rnw,
	    input [`DATA_W-1:0] 	   rw_data_to_wr,

	    //data
	    input [`N*`DATA_W-1:0] 	   data_in_bus,
	    output reg signed [`DATA_W-1:0] 	   alu_result,
	    output reg 			   c_out,

	    //config data
	    input [`ALU_CONFIG_BITS - 1:0] configdata	
	    );

   reg [`DATA_W:0] 			   ai;
   reg [`DATA_W:0] 			   bz;
   reg [2:0] 				   byte_find;
   wire [`DATA_W:0] 			   temp_adder;
   wire [5:0] 				   data_out_clz_i;
   reg 					   cin;
   wire 				   is_ra_rb_equal;

   reg signed [`DATA_W-1:0] 				   alu_result_int;

`ifdef C_USE_EXT2
   reg [`DATA_W-1:0] 				   ext2_result;
   reg 					   ext2_flag;
`endif 

`ifdef C_USE_PCMP
   reg [5:0] 				   pcmp_result;
   reg 					   pcmp_flag;
`endif 

   wire [`N_W-1: 0] 			   sela;
   wire [`N_W-1: 0] 			   selb;

   wire [`DATA_W-1:0] 		   op_a;
   wire [`DATA_W-1:0] 		   op_b;   
   wire [`ALU_FNS_W-1:0] 		   fns;
   
   wire 				   enabledA, enabledB;

   //unpack config data
   assign sela = configdata[`ALU_CONFIG_BITS-1 -: `N_W];
   assign selb = configdata[`ALU_CONFIG_BITS-`N_W-1 -: `N_W];
   assign fns = configdata[`ALU_FNS_W-1 : 0];

   //input selection 
   xinmux muxa (
		.sel(sela),
		.data_in_bus(data_in_bus),
		.data_out(op_a),
		.enabled(enabledA)
		);
   
   xinmux muxb (
		.sel(selb),
		.data_in_bus(data_in_bus),
		.data_out(op_b),
		.enabled(enabledB)		
		);
   
   
   //computes alu_result_int, c_out, ext2_result
   always @(*) begin
      
      alu_result_int = temp_adder[31:0] ;
      c_out = temp_adder[32];
`ifdef C_USE_EXT2
      ext2_result = temp_adder[31:0] ;
      ext2_flag = 1'b0;
`endif 

      case(fns)
	`ALU_LOGIC_OR : begin
	   alu_result_int = op_a | op_b;
	end
	`ALU_LOGIC_AND : begin
	   alu_result_int = op_a & op_b;
	end
	`ALU_LOGIC_ANDN : begin
	   alu_result_int = op_a &  ~op_b;
	end
	`ALU_LOGIC_XOR : begin
	   alu_result_int = op_a ^ op_b;
	end 	
	`ALU_SEXT8 : begin      
	   alu_result_int = {{24{op_a[7]}}, op_a[7:0]};
	end
	`ALU_SEXT16 : begin
	   alu_result_int = {{16{op_a[15]}}, op_a[15:0]};  
	end
	`ALU_SHIFTR_ARTH : begin
	   alu_result_int = {op_a[31] ,op_a[31:1] };
	   c_out = op_a[0] ;
	end
	`ALU_SHIFTR_LOG : begin
	   alu_result_int = {1'b 0,op_a[31:1] };
	   c_out = op_a[0] ;
	end
	`ALU_CMP_UNS : begin
	   alu_result_int[31]  = temp_adder[32] ;
	end
	`ALU_CMP_SIG : begin
	   c_out = temp_adder[32] ;
	end
	`ALU_SUB : begin
	   c_out = temp_adder[32] ;
	end
	`ALU_ADD : begin
	   c_out = temp_adder[32] ;
	end
`ifdef C_USE_EXT2
	`ALU_MAX : begin
	   ext2_flag = 1'b1;
           if(temp_adder[31]  == 1'b 0) begin
              ext2_result = op_b;
           end
           else begin
              ext2_result = op_a;
           end
	end
	`ALU_MIN : begin
	   ext2_flag = 1'b1;
           if(temp_adder[31]  == 1'b 0) begin
              ext2_result = op_a;
           end
           else begin
              ext2_result = op_b;
           end
	end

`endif //C_USE_EXT2
	default : begin
	   c_out = 1'b0;
	end
      endcase // case (fns)

      
   end

   //computes temp_adder
   assign temp_adder = ((bz & ({1'b 0,op_b}))) + ((ai ^ ({1'b 0,op_a}))) + {{32{1'b0}},cin};

   //compute ai, cin, bz
   always @(*) begin
      cin = 1'b 0;
      ai = {33{1'b0}}; // will invert op_a if set to all ones
      bz = {33{1'b1}}; // will zero op_b if set to all zeros

      case(fns)
	`ALU_CMP_UNS : begin
	   ai = {33{1'b1}};
	   cin = 1'b 1;
	end
	`ALU_CMP_SIG : begin
	   ai = {33{1'b1}};
	   cin = 1'b 1;
	end
	`ALU_SUB : begin
	   ai = {33{1'b1}};
	   cin = 1'b 1;
	end
`ifdef C_USE_EXT2
	`ALU_MAX : begin
           ai = {33{1'b1}};
           cin = 1'b 1;
	end
	`ALU_MIN : begin
           ai = {33{1'b1}};
           cin = 1'b 1;
	end
	`ALU_ABS : begin
           if(op_a[31]  == 1'b 1) begin
              // ra is negative
              ai = {33{1'b1}};
              cin = 1'b 1;
           end
           bz = {33{1'b0}};
	end
`endif //C_USE_EXT2
	default : begin
	end
      endcase
   end

`ifdef C_USE_PCMP
   // byte find 
   always @(fns or byte_find or is_ra_rb_equal or data_out_clz_i) begin
      pcmp_result = {6{1'b0}};
      
      pcmp_flag = 1'b1;
      
      case(fns)
	`ALU_PCBF : begin
           pcmp_result = {3'b 000,byte_find};
	end
	`ALU_PCE : begin
           pcmp_result = {5'b 00000,is_ra_rb_equal};
	end
	`ALU_PCNE : begin
           pcmp_result = {5'b 00000, ~is_ra_rb_equal};
	end
	`ALU_PCCLZ : begin
           pcmp_result = data_out_clz_i;
	end
	default : begin
	   pcmp_flag = 1'b0;
	end
      endcase
   end // always @ (fns or byte_find or is_ra_rb_equal or data_out_clz_i)
   
   always @(op_a or op_b) begin
      if((op_a[31:24]  == op_b[31:24] )) begin
         byte_find = 3'b 001;
      end
      else if((op_a[23:16]  == op_b[23:16] )) begin
         byte_find = 3'b 010;
      end
      else if((op_a[15:8]  == op_b[15:8] )) begin
         byte_find = 3'b 011;
      end
      else if((op_a[7:0]  == op_b[7:0] )) begin
         byte_find = 3'b 100;
      end
      else begin
         byte_find = 3'b 000;
      end
   end
   // count leading zeros
   xclz clz(
            .data_in(op_a),
	    .data_out(data_out_clz_i));
`endif

`ifdef C_USE_PCMP
   assign is_ra_rb_equal = op_a == op_b? 1'b1 : 1'b0;
`endif

   always @ (posedge clk)
     if (rst)
       alu_result <= `DATA_W'h00000000;
     else begin
	if ( rw_req & ~rw_rnw )
	  alu_result <= rw_data_to_wr;
	else if(enabledA & enabledB )
	  if (pcmp_flag)
	    alu_result <= pcmp_result;
	  else if (ext2_flag)
	    alu_result <= ext2_result;
	  else 
	    alu_result <= alu_result_int;
     end			
endmodule
