/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

module xclz(
    input [31:0] data_in,
    output reg [5:0] data_out
    );

	/*reg [6:0]*/integer i;
	reg mark;
	
/*	always @ (data_in) begin 
		mark = 1'b0;
		data_out = 32;
		for ( i = 32; i > 0; i = i - 1)  
			if ( data_in[i-1] == 1'b1 && mark == 1'b0) begin 
				mark = 1'b1;
				data_out = 31-i+1;
			end
	end 
*/

		always @ * begin
			if(data_in[31] == 1'b1)
				data_out = 0;
			else if(data_in[31:30] == 2'b01)
				data_out = 1;
			else if(data_in[31:29] == 3'b001)
				data_out = 2;
			else if(data_in[31:28] == 4'b0001)
				data_out = 3;
			else if(data_in[31:27] == 5'b00001)
				data_out = 4;
			else if(data_in[31:26] == 6'b000001)
				data_out = 5;
			else if(data_in[31:25] == 7'b0000001)
				data_out = 6;
			else if(data_in[31:24] == 8'b00000001)
				data_out = 7;
			else if(data_in[31:23] == 9'b000000001)
				data_out = 8;
			else if(data_in[31:22] == 10'b0000000001)
				data_out = 9;
			else if(data_in[31:21] == 11'b00000000001)
				data_out = 10;
			else if(data_in[31:20] == 12'b000000000001)
				data_out = 11;
			else if(data_in[31:19] == 13'b0000000000001)
				data_out = 12;
			else if(data_in[31:18] == 14'b00000000000001)
				data_out = 13;
			else if(data_in[31:17] == 15'b000000000000001)
				data_out = 14;
			else if(data_in[31:16] == 16'b0000000000000001)
				data_out = 15;
			else if(data_in[31:15] == 17'b00000000000000001)
				data_out = 16;
			else if(data_in[31:14] == 18'b000000000000000001)
				data_out = 17;
			else if(data_in[31:13] == 19'b0000000000000000001)
				data_out = 18;
			else if(data_in[31:12] == 20'b00000000000000000001)
				data_out = 19;
			else if(data_in[31:11] == 21'b000000000000000000001)
				data_out = 20;
			else if(data_in[31:10] == 22'b0000000000000000000001)
				data_out = 21;
			else if(data_in[31:9] == 23'b00000000000000000000001)
				data_out = 22;
			else if(data_in[31:8] == 24'b000000000000000000000001)
				data_out = 23;
			else if(data_in[31:7] == 25'b0000000000000000000000001)
				data_out = 24;
			else if(data_in[31:6] == 26'b00000000000000000000000001)
				data_out = 25;
			else if(data_in[31:5] == 27'b000000000000000000000000001)
				data_out = 26;
			else if(data_in[31:4] == 28'b0000000000000000000000000001)
				data_out = 27;
			else if(data_in[31:3] == 29'b00000000000000000000000000001)
				data_out = 28;
			else if(data_in[31:2] == 30'b000000000000000000000000000001)
				data_out = 29;
			else if(data_in[31:1] == 31'b0000000000000000000000000000001)
				data_out = 30;
//			else if(data_in[31:0] == 32'b00000000000000000000000000000001)
//				data_out = 31;
			else data_out = 31;
		end
endmodule
