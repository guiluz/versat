/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

module xmux16x1x32(
    input [3:0] sel,
    input [16*32-1:0] a,
    output reg [31:0] c
    );

always @ (*) begin 
	case (sel)
		0: c = a[1*32-1: 0*32];
		1: c = a[2*32-1: 1*32];
		2: c = a[3*32-1: 2*32];
		3: c = a[4*32-1: 3*32];
		4: c = a[5*32-1: 4*32];
		5: c = a[6*32-1: 5*32];
		6: c = a[7*32-1: 6*32];
		7: c = a[8*32-1: 7*32];
		8: c = a[9*32-1: 8*32];
		9: c = a[10*32-1: 9*32];
		10: c = a[11*32-1: 10*32];
		11: c = a[12*32-1: 11*32];
		12: c = a[13*32-1: 12*32];
		13: c = a[14*32-1: 13*32];
		14: c = a[15*32-1: 14*32];
		15: c = a[16*32-1: 15*32];	
	endcase
end 

endmodule
