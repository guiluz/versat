/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`define C_USE_MUL
`define C_USE_DIV             
`define C_USE_BS               
`define C_USE_PCMP       		
`define C_USE_EXT0      
//`define C_USE_EXT1            
`define C_USE_EXT2           

`define C_DEBUG

// parameter dependencies below this line: please DON'T edit

`ifdef C_USE_EXT0
   `define C_USE_MUL
`endif
