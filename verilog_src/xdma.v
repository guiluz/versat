/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"


module xdma(
    input clk ,
    input rst,

	//xctrl interface 
	 input      [`DATA_W-1:0]     data_to_wr,
	 input						  rw_req,
	 input      [`INT_ADDR_W-1:0] rw_addr,
	 output     [`DATA_W-1:0]     data_to_rd,
	 
	//external interface 
	 input                        ext_ack,
   	 output reg         	  	  ext_req,
	 output reg	        	   	  ext_cyc,
	 output reg        		   	  ext_rnw,
	 output reg  [`DATA_W-1:0]    ext_addr,
     input       [`DATA_W-1:0]    ext_data_in,
     output      [`DATA_W-1:0]    ext_data_out,
	 output      [3:0]  		  ext_sel,
	 output	     [1:0]  		  ext_bte,
	 output	reg  [2:0]  		  ext_cti,
	 
	//internal interface 
	 input						   mem_rdy,
	 output reg    	    		   int_en,
     output reg 	    		   int_rnw,
     output      [`INT_ADDR_W-2:0] int_addr,
     input       [`DATA_W-1:0] 	   int_data_in,
     output	     [`DATA_W-1:0] 	   int_data_out,
	 output 	 [`ADDR_W-1:0] 	   mem_addr,
	 output	reg	     			   inst_mem_we,
	 output reg	 				   conf_req,
     output	reg					   conf_rnw

	 );	
	
	//state machine
	 parameter 				  IDLE=2'b00 , CMD=2'b01, WM=2'b10, RUN=2'b11;
	 reg [1:0] 				  state, state_nxt;
	 reg [`TRFSZ_W-1:0]       size, size_nxt;
	 reg [`INT_ADDR_W-1:0]	  int_addr_temp,int_addr_temp_nxt;
	 reg					  dma_rdy;
	 reg 					  dma_rdy_nxt;
	 reg					  ext_req_nxt;
	 reg					  ext_cyc_nxt;
	 reg					  ext_rnw_nxt;
	 reg [`DATA_W-1:0]   	  ext_addr_nxt;
	 reg					  int_en_nxt;
	 reg					  int_rnw_nxt;
	 reg [2:0]     			  ext_cti_nxt;
	 reg					  inst_mem_we_nxt;
	 reg 					  conf_rnw_nxt;	
	 reg 					  conf_req_nxt;	

 	 reg					  temp_dir;


	 wire					  ctrl_req;
	 wire 					  ext_addr_req;
     assign ctrl_req = (rw_addr == `DMA_CTRL_REG)? rw_req : 1'b0;
   	 assign ext_addr_req = (rw_addr == `DMA_EXT_ADDR)? rw_req : 1'b0;
     assign data_to_rd = {{(`DATA_W-1){1'b0}},dma_rdy};	
	 
	 assign int_addr= (temp_dir)? int_addr_temp_nxt[12:0] : int_addr_temp[12:0];
	 assign ext_sel=4'b1111;
	 assign ext_data_out=int_data_in;
	 assign int_data_out=ext_data_in;
	 assign mem_addr=int_addr[`ADDR_W-1:0];
	 assign ext_bte=2'b00;
	
always @ (posedge clk,posedge rst) begin
	if (rst) begin
	   ext_req<=1'b0;
	   ext_cyc<=1'b0;
	   int_en<=1'b0;
	   ext_rnw<=1'b1;
	   int_rnw<=1'b1;
	   dma_rdy<=1'b1;
	   inst_mem_we<=1'b0;
	   conf_req<=1'b0;
	   conf_rnw<=1'b1;
	   state<=IDLE;
	end 
	else begin
	   state<=state_nxt;
 	   size<=size_nxt;
 	   int_en<=int_en_nxt;
       ext_req<=ext_req_nxt;
	   ext_cyc<=ext_cyc_nxt;
       dma_rdy<=dma_rdy_nxt;	     
	   ext_rnw<=ext_rnw_nxt;
	   int_rnw<=int_rnw_nxt;
	   ext_addr<=ext_addr_nxt;
       int_addr_temp<=int_addr_temp_nxt;
	   ext_cti<=ext_cti_nxt;
	   inst_mem_we<=inst_mem_we_nxt;
	   conf_rnw<=conf_rnw_nxt;
	   conf_req<=conf_req_nxt;
    end   
end
 always @ (*) begin

      state_nxt = state;
      ext_req_nxt=ext_req;
      ext_cyc_nxt=ext_cyc;
      int_en_nxt=int_en;
      ext_rnw_nxt=ext_rnw;
      int_rnw_nxt=int_rnw;
      dma_rdy_nxt=dma_rdy;
	  inst_mem_we_nxt=inst_mem_we;
	  conf_rnw_nxt=conf_rnw;
	  conf_req_nxt=conf_req;
     
	 if (state==IDLE & ctrl_req) begin
		state_nxt=CMD;
		dma_rdy_nxt=1'b0;
		size_nxt=data_to_wr[`TRFSZ_OFFSET-1-:`TRFSZ_W];
		int_addr_temp_nxt=data_to_wr[`INT_ADDR_W-1:0];
		temp_dir=data_to_wr[24];
		if (size_nxt==0)
      	ext_cti_nxt=3'b000;
    	else
    		ext_cti_nxt=3'b010;	
	 end
	 if(state==CMD & ext_addr_req) begin
		ext_addr_nxt={4'b0,data_to_wr[`DATA_W-5:0]};
		if (int_addr_temp[13:11]==3'b001) begin // write to instruction memory
			state_nxt=RUN;
			inst_mem_we_nxt=1'b1;
			ext_req_nxt=1'b1;
  			ext_cyc_nxt=1'b1;
			temp_dir=1'b0;
		end else if (int_addr_temp[13:11]==3'b011) begin // write/read to config memory
			state_nxt=RUN;
			inst_mem_we_nxt=1'b1;
			ext_req_nxt=1'b1;
			ext_cyc_nxt=1'b1;
			conf_req_nxt=1'b1;
			if (temp_dir)
      			ext_rnw_nxt=1'b0;
   			else
				conf_rnw_nxt=1'b0; 		
		end else if (int_addr_temp[13]==1'b1) begin// write/read to data_eng
			state_nxt=WM;	
		end else

			$display ("Incorrect internal address: %d", int_addr_temp);
 	 end	 	   		
	 if(state==WM && mem_rdy) begin
 		state_nxt=RUN;
		ext_req_nxt=1'b1;
  		ext_cyc_nxt=1'b1;
		int_en_nxt=1'b1;
		if (temp_dir)
  			ext_rnw_nxt=1'b0;
		else
			int_rnw_nxt=1'b0;
	 end 

      if(state==RUN & ext_ack==1) begin
		  if (size>0) begin
			size_nxt=size-1;
			ext_addr_nxt=ext_addr+4;
			int_addr_temp_nxt=int_addr_temp+1;
		  end
		  if (size==1)
			 ext_cti_nxt=3'b111;
		  if (size==0) begin
	         inst_mem_we_nxt=1'b0;
			 ext_rnw_nxt=1'b1;
			 int_rnw_nxt=1'b1;	
			 ext_cyc_nxt=1'b0;             			 
		     int_en_nxt=1'b0;
			 dma_rdy_nxt=1'b1;
			 ext_req_nxt=1'b0;

			 conf_rnw=1'b1;
			 conf_req=1'b0;
			 state_nxt=IDLE;
		  end
	 end
end

endmodule
