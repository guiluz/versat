/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xboot_rom(
		 input 			 clk,
		 input 			 en,
		 input 			 req,
		 input [`ROM_ADDR_W-1:0] addr,
		 output reg [`DATA_W-1:0] 	 data
		 );
   
   wire 		   read;
   
   reg [`DATA_W-1:0] mem [2**`ROM_ADDR_W-1:0];  
   
   assign read = en & req;

   initial begin
      $readmemh("../assembly_src/boot_rom.hex",mem,0,254);
   end

   always @ (posedge clk) begin
      if (read)
	data <= mem[addr];  
   end
   

   
endmodule
