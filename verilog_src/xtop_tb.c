/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Guilherme Luz <gui_luz_93@hotmail.com>
 
 ***************************************************************************** */

#include <vpi_user.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define NBITS  38
#define NINST  166 //Instructions number (change here)
#define NWORDS (2*NINST+2)
int **aloc_opcode_map(int rows, int cols){
	int i;
	int **mat = (int **)calloc(rows , sizeof(int*));
	for(i = 0; i < rows; i++) 
		mat[i] = (int *)calloc(cols , sizeof(int));
	return mat;
}

int **hex_to_bin(int **mat, FILE *fp){
	int i;
	int k;
	int m=0;
	char bin_word[5];
	char word[9];
	char addr[5];

	/*write LOAD_ADDR=8 to PROG_ADDR_REG*/
		for(k=0;k<38;k++)
			if( k==34)
				mat[m][k]=1;
			else
				mat[m][k]=0;
		m++;	
		

	while(fscanf(fp,"%s",word)==1 ){
		i=0;
		k=0;
		/*rnw bit*/
		mat[m][4*i+k]=0;
		/*Convert addr to binary, to write to or read from register bank*/
		strcpy(addr,"0010"); /*change address here*/
		for(k=0;k<4;k++)
				if(addr[k]=='0')
					mat[m][4*i+2+k]=0;
				else
					mat[m][4*i+2+k]=1;
		/*Convert data to binary*/
		for(i=0; i<8; i++){
			switch(word[i]){
		         case '0': strcpy(bin_word,"0000"); break;
		         case '1': strcpy(bin_word,"0001"); break;
		         case '2': strcpy(bin_word,"0010"); break;
		         case '3': strcpy(bin_word,"0011"); break;
		         case '4': strcpy(bin_word,"0100"); break;
		         case '5': strcpy(bin_word,"0101"); break;
		         case '6': strcpy(bin_word,"0110"); break;
		         case '7': strcpy(bin_word,"0111"); break;
		         case '8': strcpy(bin_word,"1000"); break;
		         case '9': strcpy(bin_word,"1001"); break;
		         case 'A': strcpy(bin_word,"1010"); break;
		         case 'B': strcpy(bin_word,"1011"); break;
		         case 'C': strcpy(bin_word,"1100"); break;
		         case 'D': strcpy(bin_word,"1101"); break;
		         case 'E': strcpy(bin_word,"1110"); break;
		         case 'F': strcpy(bin_word,"1111"); break;
		         case 'a': strcpy(bin_word,"1010"); break;
		         case 'b': strcpy(bin_word,"1011"); break;
		         case 'c': strcpy(bin_word,"1100"); break;
		         case 'd': strcpy(bin_word,"1101"); break;
		         case 'e': strcpy(bin_word,"1110"); break;
		         case 'f': strcpy(bin_word,"1111"); break;
		         default:  printf("\nInvalid hexadecimal digit %c ",word[i]); return 0;
         		}
			for(k=0;k<4;k++){
				if(bin_word[k]=='0')
					mat[m][4*(i+1)+2+k]=0;
				else
					mat[m][4*(i+1)+2+k]=1;
			}
		}
		m++;

		/*write 1 to NEW_WORD_STATUS_REG*/
		for(k=0;k<38;k++)
			if(k==5 || k==37)
				mat[m][k]=1;
			else
				mat[m][k]=0;
		m++;		
	}
	
		/*write PROG_START to PROG_ADDR_REG */
		for(k=0;k<38;k++)
			if( k==29)
				mat[m][k]=1;
			else
				mat[m][k]=0;
		m++;	

	return mat;
}

void printf_mat(int **mat){
	int i;
	int k;
	for(i=0; i<NWORDS; i++){
		printf("linha:%d\n",i);
		for(k=0; k<NBITS; k++){
			printf("%d",mat[i][k]);
			if((k+1+2)%4==0)
				printf(" ");
		}
		printf("\n");
	}
}

// Implements the system task
static int rw_calltf (char *userdata) {
  vpiHandle systfref, args_iter, argh;
  struct t_vpi_value argval;
  int value;
  static int sclk, ss, mosi, getdata;
  static int **data;
  static FILE *fread;
  int i;
  static int m,n,k; 
  // Obtain a handle to the argument list
  systfref = vpi_handle(vpiSysTfCall, NULL);
  args_iter = vpi_iterate(vpiArgument, systfref);

  if(getdata==0){
  	fread=fopen("../assembly_src/opcode.hex", "r");
	if(fread==NULL){
		printf("ERROR: fopen\n"); exit(0);
	}
	data=aloc_opcode_map(NWORDS, NBITS);

  	data=hex_to_bin(data,fread);
	fclose(fread);
	//printf_mat(data);
	getdata=1;
	k=1;
  }
  // Grab the value of the first argument
  for(i=0; i<3;i++){
	  argh = vpi_scan(args_iter);
	  argval.format = vpiIntVal;
	  vpi_get_value(argh, &argval); 
	  value = argval.value.integer;
	  // Increment the value and put it back as first argument
	  switch(i){
		case(0): //sclk
			sclk=!value;
		  	argval.value.integer=sclk;
		break;
		
		case(1): //ss
			ss=value;
			if(sclk==0){
				if(k==1){
					ss=0;
				}else if(k==7){
					ss=1;
				}else if(k==8){
					ss=0;
				}else if(k==40){
					ss=1;
					k=0;
				}
				k++;
			}	
				argval.value.integer=ss;						
		break;
		
		case(2): //mosi
			if(sclk==0 && ss==0 && n<NWORDS ){
				//printf("mosi=data[%d][%d]=%d\n", n,m,data[n][m]);
				mosi=data[n][m];			
				m++;
			}else
				mosi=value;
			if(k==1){
				if(m==38){
					printf("%d\n", n);
					n++;
				}
				m=0;
			}
			argval.value.integer=mosi;
		break;

	  }
	  vpi_put_value(argh, &argval, NULL, vpiNoDelay);
  }

  // Cleanup and return
  vpi_free_object(args_iter);
  return 0;

}

void rw_register()
{
      s_vpi_systf_data tf_data;

      tf_data.type      = vpiSysTask;
      tf_data.tfname    = "$vpi_rw";
      tf_data.calltf    = rw_calltf;
      tf_data.compiletf = 0;
      tf_data.sizetf    = 0;
      tf_data.user_data = 0;
      vpi_register_systf(&tf_data);
}

void (*vlog_startup_routines[])() = {
    rw_register,
    0
};
