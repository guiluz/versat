`timescale 1ns / 1ps
module test_tb;
   parameter clk_per=10;
   
   integer i;

   reg sclk=0;
   reg ss=1;
   wire  miso;
   reg mosi;
   initial begin
      $dumpfile("test.vcd");
      $dumpvars();
      for (i = 0; i < 30; i=i+1) begin

	 $vpi_rw(sclk, ss, mosi, miso);
	 
	 //$display("Clock: %2d \n", sclk );	 
	 //$display("ss : %2d \n", ss);
	 #(clk_per/2);
      end	
      
   end
   

endmodule // tb
