#include <vpi_user.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// Implements the system task
static int rw_calltf (char *userdata) {
  vpiHandle systfref, args_iter, argh;
  struct t_vpi_value argval;
  int value;
  static int sclk, ss;
  int i;
  static int k; 
  // Obtain a handle to the argument list
  systfref = vpi_handle(vpiSysTfCall, NULL);
  args_iter = vpi_iterate(vpiArgument, systfref);

  //printf("k=%d\n\n", k);
  // Grab the value of the first argument
  for(i=0; i<4;i++){
	  argh = vpi_scan(args_iter);
	  //argval.format = vpiIntVal;
	  vpi_get_value(argh, &argval); 
	  value = argval.value.integer;
	  //vpi_printf("VPI routine received %2d\n", value);
	  
	  // Increment the value and put it back as first argument
	  switch(i){
		case(0): //clk
			vpi_printf("VPI clock received %2d\n", value);
			sclk=!value;
		  	argval.value.integer=sclk;
		break;
		case(1): //ss
			vpi_printf("VPI ss received %2d\n", value);
			
			if((k<10 && ss==0) || sclk==1){
				ss=0;
				argval.value.integer=ss;
				k++;
			}else{
				k=0;
				ss=1;
				argval.value.integer=ss;
			}
				
		break;
		case(2): //mosi
			vpi_printf("VPI mosi received %2d\n", value);
			if(sclk==0)
				argval.value.integer=random()%2;
			else
				argval.value.integer=value;

		break;
		case(3): //miso
			vpi_printf("VPI miso received %2d\n\n", value);
			if(sclk==0)
				argval.value.integer=random()%2;
			else
				argval.value.integer=value;
		break;
	  }
	  vpi_put_value(argh, &argval, NULL, vpiNoDelay);
  }
  // Cleanup and return
  vpi_free_object(args_iter);
  return 0;

}

void rw_register()
{
      s_vpi_systf_data tf_data;

      tf_data.type      = vpiSysTask;
      tf_data.tfname    = "$vpi_rw";
      tf_data.calltf    = rw_calltf;
      tf_data.compiletf = 0;
      tf_data.sizetf    = 0;
      tf_data.user_data = 0;
      vpi_register_systf(&tf_data);
}

void (*vlog_startup_routines[])() = {
    rw_register,
    0
};
