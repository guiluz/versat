\documentclass{rep}

\title{Versat Specification}
\category{Report}
%\confidential{}

\begin{document}
\maketitle
\cleardoublepage
\tableofcontents
\cleardoublepage
\listoftables
\cleardoublepage
\listoffigures
\cleardoublepage

\section{Introduction}
\label{sec:intro}

Versat is a computing engine whose architecture can be described as a
Coarse Grain Array (CGA) or Soft Vector Processor (SVP). A high-level
diagram is shown in Figure~\ref{fig:versat_arch}.

Versat can be included as an Intellectual Property (IP) core in more
complex designs that require intense data processing. Currently it has
a Serial Peripheral Interface (SPI) used to control the core from the
outside. It has a Wishbone~\cite{wishbone} interface to access
external memory.

The Controller of the Versat machine executes instructions from the
Program Memory. The Program Memory consists of a Boot ROM and an
Execution RAM. The Boot ROM runs initially to load the program either
from SPI or from external memory into the Execution RAM. The Boot ROM
program exits by jumping to the first instruction in the Execution
RAM.

The Controller issues commands and reads statuses from the blocks in
the system by means of the Read/Write (R/W) bus. The Controller
receives commands and issues status information to the Control
Register File, which can be read and written by SPI.

The Data Engine is where computation takes place. Computation is
carried out on data arrays (vectors) stored in internal vector
RAMs. The conventional computer equivalent of vector RAMs is the
register file used to hold scalar quantities. The vector RAMs are
dual-port and each port is equipped with an Address Generation Unit
(AGU) used to read or write the vector elements in a variety of
address sequence patterns. The Data Engine also has a number of
Functional Units (FUs) to process the data in a pipeline fashion. In
essence, the data is streamed out of the vector RAMs, goes through
variable length of processing pipelines, and the result vectors are
streamed back into the vector RAMs. The AGUs and FUs are
configurable. The interconnection between RAMs and FUs and between FUs
is also configurable. Configuration data is hold in the Configuration
Memory.

The instruction set of the Controller is minimal since most of the
computation happens in the Data Engine. The instructions are used
chiefly to move data, including the program itself, data vectors and
configuration data. The instructions are also used to inspect data and
for decision taking. Data movement uses a Direct Memory Access (DMA)
unit for efficiency.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=16cm]{../drawings/top.png}}
    \vspace{0cm}\caption{The Versat architecture.}
    \label{fig:versat_arch}
\end{figure}


\section{Background}
\label{sec:background}

\section{Architecture}
\label{sec:arch}

In this section a detailed architecture description is
presented. Figure \ref{fig:versat_arch} provides the top-level view
and the next subsections explain each submodule.

\subsection{Data Engine}
\label{sec:de}

A high-level diagram of the Data Engine module is shown in Figure
\ref{fig:de}. The Data Engine is a collection of units of different
types that draw input signals from a wide data bus formed by the
outputs of all units. A graphical representation of the data bus is
given in Figure \ref{fig:databus}. Currently the Data Engine has 4
Arithmetic and Logic Units (ALU), 4 fixed point Multipliers (MUL) and
2 Barrel Shifters (BS).

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=12cm]{../drawings/de.png}}
    \vspace{0cm}\caption{The Data Engine.}
    \label{fig:de}
\end{figure}

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=18cm]{../drawings/databus.png}}
    \vspace{0cm}\caption{The Data Bus.}
    \label{fig:databus}
\end{figure}

\subsection{Controller}
\label{sec:control}

The controller executes machine code instructions. The instruction set
is explained in Section~\ref{sec:isa}. The instructions are stored in
the Program Memory block described in Section~\ref{sec:progmem}. The
controller sees the other blocks in the system as memory mapped, and
uses the R/W bus to monitor their status and issue commands to
them.

The architecture of the controller is depicted in
Figure~\ref{fig:control}. The controller has 3 main registers: the
Acummulator (A), the Program Counter (PC) and the Pointer (B). 

The PC addresses the Program Memory causing instructions to be read
from it and decoded in the controller. The PC normally increments to
fetch the next intruction or, to implement program jumps, it is loaded
with an instruction immediate constant or a memory mapped value.

The Accumulator performs arithmetic or logic operations between itself
and an input which can be an instruction immediate or a memory mapped
value.

The Pointer is loaded with a memory mapped value which represents a
memory address to where one can write or read. The Pointer register is
the way the controller implements indirect addressing.


\begin{figure}[!htbp]
    \centerline{\includegraphics[width=16cm]{../drawings/control.png}}
    \vspace{0cm}\caption{The Controller.}
    \label{fig:control}
\end{figure}

\subsection{Program Memory}
\label{sec:progmem}

\subsection{Control Register File}
\label{sec:crf}

The Control Register File (CRF) is shown in Figure~\ref{fig:crf}. Its function is twofold: (1) provide command interface between Versat and a user block and (2) provide a general purpose register bank for use by Versat programs.

It consists of 16 32-bit registers, CRF[0] to CRF[15]. Register CRF[0]
is special in the sense that it is where users indicate the address of
the Versat program to be run. When this register is reset it means that Versat is idle. If CRF[0] is set it means Versat is busy. CRF[0] is set by the user and reset by Versat on completion of the program.


Registers CRF[1] to CRF[15] can be used by the user to pass program
arguments to Versat programs. During execution Versat programs can use CRF as a general purpose register bank. However, they should never reset CRF[0] as the would be wrongly tell the user that program has finished. Only on program completion shall CRF[0] be reset by a Versat program.


\begin{figure}[!htbp]
    \centerline{\includegraphics[width=16cm]{../drawings/ctrf.png}}
    \vspace{0cm}\caption{Control Register File.}
    \label{fig:crf}
\end{figure}


\subsection{Configuration Memory}
\label{sec:confmem}

\subsection{Direct Memory Access}
\label{sec:dma}

\subsection{Serial Peripheral Interface}
\label{sec:spi}


\section{Instruction Set}
\label{sec:isa}

The instruction set is given in Table \ref{tab:isa}. Versat features a
very minimal set of instructions to configure and control the execution
in the data engine.

There is only one instruction type which consists of
4-bit opcode and a 28-bit immediate constant (Imm) as shown in Figure~\ref{fig:if}.
\begin{figure}[!htbp]
    \centerline{\includegraphics[width=16cm]{../drawings/if.png}}
    \vspace{0cm}\caption{Instruction Format.}
    \label{fig:if}
\end{figure}

The program counter (PC) hard resets at position 0, where a boot
loader program resides. The PC increments after each instruction, and
jumps to target values after flow control instructions {\tt beqi, beq,
  bneqi, bneq} and {\tt stop}. The boot loader is explained in section
\ref{sec:bootloader}.

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{2cm}|p{2cm}|p{7cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Opcode} & {\bf Description} \\
    \hline \hline 
    nop & 0x0 & No operation; PC = PC+1. \\
    \hline
    rdw & 0x1 & A = (Imm); PC = PC+1. \\
    \hline
    rdwb & 0x2 & B = (Imm); PC = PC+1. \\
    \hline
    wrw & 0x3 & (Imm) = A; PC = PC+1. \\
    \hline
    wrwb & 0x4 & (B) = A; B = B+1; PC = PC+1. \\
    \hline
    beqi & 0x5 & A == 0? PC = Imm: PC = PC+1; A = A-1\\
    \hline
    beq & 0x6 & A == 0? PC = (Imm): PC = PC+1; A = A-1\\
    \hline
    bneqi & 0x7 & A != 0? PC = Imm: PC = PC+1; A = A-1\\
    \hline
    bneq & 0x8 & A != 0? PC = (Imm): PC = PC+1; A = A-1\\
    \hline
    ldi & 0x9 & A = Imm; PC=PC+1\\
    \hline
    ldih & 0xA & A[31:0] = Imm[15:0]; PC=PC+1\\
    \hline
    add & 0xB & A = A+(Imm); PC=PC+1\\
    \hline
    addi & 0xC & A = A+Imm; PC=PC+1\\
    \hline
    sub & 0xD & A = A-(Imm); PC=PC+1\\
    \hline
    and & 0xE & A = A\&(Imm); PC=PC+1\\
    \hline
    stop & 0xF & CRF[0]=0; PC=0\\
    \hline
 
    \end{tabular}
  \caption{Instruction Set}
  \label{tab:isa}
\end{table}

Brackets are used to represent memory pointers. CRF stands for Control
Register File explained in section~\ref{sec:crf}.

\section{Boot Loader Program}
\label{sec:bootloader}



\bibliographystyle{unsrt}
\bibliography{rep}
%\nocite{*}

\end{document}
