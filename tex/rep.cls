\LoadClass[twoside,a4paper]{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{djdoc}

\RequirePackage{graphicx}
\RequirePackage{helvet}
\RequirePackage{fancyhdr}
\RequirePackage[includehead,includefoot,top=50pt,bottom=50pt,headheight=24pt]{geometry}
\RequirePackage{hyperref}

\pagestyle{fancy}

\renewcommand{\headrulewidth}{.4pt}
\fancyhead{}
\fancyhead[RO,LE]{
\textbf{\@title}
\\
\textsc{\small{\@category}}
}
%\fancyhead[RE,LO]{\includegraphics[scale=.5]{DjungaLogo.pdf}}

\newcommand{\confidential}{\def\@confidential}
\renewcommand{\footruleskip}{10pt}
\renewcommand{\footrulewidth}{.4pt}
\fancyfoot{}
\fancyfoot[RO,LE]{
\fancyfoot[C]{\url{www.inesc-id.pt}}
\fancyfoot[RE,LO]{\@ifundefined{@confidential}{}{\textbf{Confidential}}}
\thepage
}

\newcommand{\category}[1]{\def\@category{#1}}
\renewcommand{\familydefault}{\sfdefault}

\renewcommand{\maketitle}{
\begin{titlepage}
\setlength{\parindent}{0pt}
\setlength{\parskip}{1ex}
\vspace*{100pt}
\Huge{\textbf{\@title}}

\huge{\@category}

\vspace*{2ex}
\includegraphics[keepaspectratio,scale=.7]{InescID_logo-s-1103x539.png}

\small{\today}
\vspace*{\fill}
\end{titlepage}
}
